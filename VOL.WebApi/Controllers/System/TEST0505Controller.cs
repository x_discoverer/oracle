/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹TEST0505Controller编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using VOL.System.IServices;
namespace VOL.System.Controllers
{
    [Route("api/TEST0505")]
    [PermissionTable(Name = "TEST0505")]
    public partial class TEST0505Controller : ApiBaseController<ITEST0505Service>
    {
        public TEST0505Controller(ITEST0505Service service)
        : base(service)
        {
        }
    }
}

