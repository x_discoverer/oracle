/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下TEST0505Service与ITEST0505Service中编写
 */
using VOL.System.IRepositories;
using VOL.System.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace VOL.System.Services
{
    public partial class TEST0505Service : ServiceBase<TEST0505, ITEST0505Repository>
    , ITEST0505Service, IDependency
    {
    public TEST0505Service(ITEST0505Repository repository)
    : base(repository)
    {
    Init(repository);
    }
    public static ITEST0505Service Instance
    {
      get { return AutofacContainerModule.GetService<ITEST0505Service>(); } }
    }
 }
